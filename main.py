from fastapi import FastAPI
from database import models
from database.database import engine
from routers import post, user, role, user_role
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
from auth import authentication


app = FastAPI()
app.include_router(authentication.router)
app.include_router(role.router)
app.include_router(user.router)
app.include_router(post.router)
app.include_router(user_role.router)
models.Base.metadata.create_all(bind=engine)

app.mount('/images', StaticFiles(directory='images'), name='images')

origins = [
  'http://localhost:3000',
  'http://localhost:3001'
]

app.add_middleware(
  CORSMiddleware,
  allow_origins=origins,
  allow_credentials=True,
  allow_methods=['*'],
  allow_headers=['*']
)