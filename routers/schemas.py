from pydantic import BaseModel
from datetime import datetime
from typing import List
from enum import Enum


class Roles(str, Enum):
    user = "user"
    admin = "admin"

class Post(BaseModel):
  image_url: str
  title: str
  content: str
  class Config():
        orm_mode = True

class UserBase(BaseModel):
    author: str
    username: str
    email: str
    password: str
    role: Roles = "user"

class UserDisplay(BaseModel):
    id: int
    username: str
    password: str
    email: str
    class Config():
        orm_mode = True

class User(BaseModel):
    id: int
    username: str
    class Config():
        orm_mode = True

class PostBase(BaseModel):
  image_url: str
  title: str
  content: str
  creator: str

class PostDisplay(BaseModel):
  id: int
  image_url: str
  title: str
  content: str
  creator: str
  timestamp: datetime
  class Config():
    orm_mode = True

class RoleBase(BaseModel):
    id: int
    role_name: str

class Role(BaseModel):
    role_name: str

class UserRole(BaseModel):
    user_id: int
    role_id: int
