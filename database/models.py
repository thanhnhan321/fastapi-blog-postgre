from routers.schemas import Roles
from .database import Base
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy import Enum

class DbUser(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True, index=True) 
    username = Column(String)
    email = Column(String)
    password = Column(String)
    author = Column(String)
    role = Column(Enum(Roles), default="user")

class DbPost(Base):
  __tablename__ = "post"
  id = Column(Integer, primary_key=True, index=True)
  image_url = Column(String)
  title = Column(String)
  content = Column(String)
  creator = Column(String)
  timestamp = Column(DateTime)

class DbRole(Base):
    __tablename__ = 'roles'
    id = Column(Integer, primary_key=True, index=True)
    role_name = Column(String, unique=True, index=True)
    description = Column(String)

class DbUserRole(Base):
    __tablename__ = 'user_roles'
    idx = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer)
    role_id = Column(Integer)
