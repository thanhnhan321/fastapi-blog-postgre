from sqlalchemy.orm.session import Session
from database.hash import Hash
from routers.schemas import UserBase
from database.models import DbUser, DbRole, DbUserRole
from fastapi import HTTPException, status

def create_user(db: Session, request: UserBase):
    new_user = DbUser(
        author= request.author,
        username= request.username,
        email= request.email,
        password= Hash.bcrypt(request.password)
    )
    role = db.query(DbRole).filter(DbRole.role_name == request.author).first()
    new_user.role = role
    
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user

# Query tất cả dữ liệu người dùng
def get_all_users(db: Session):
    return db.query(DbUser).all()

# Query 1 dữ liệu người dùng
def get_user(db: Session, id: int):
    user = db.query(DbUser).filter(DbUser.id == id).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
            detail=f'User with {id} not found')
    return user

# Query 1 dữ liệu người dùng
def get_user_by_username_role(db: Session, username: str):
    user = db.query(DbUser).filter(DbUser.username == username).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
            detail=f'User username {username} not found')
    
    user_role = (
    db.query(DbUserRole)
    .join(DbUser, DbUser.id == DbUserRole.user_id)
    .join(DbRole, DbRole.id == DbUserRole.role_id)
    .filter(DbUserRole.role_id == 1, DbUserRole.role_id == user.id)
    .all()
)
    if not user_role:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
            detail=f'You do not have permission to access this route')
    return user_role

# Update one user
def update_user(db: Session, id: int, request: UserBase):
    user = db.query(DbUser).filter(DbUser.id == id)
    if not user.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
            detail=f'User with {id} not found')
    user.update({
        DbUser.username: request.username,
        DbUser.email: request.email,
        DbUser.password: Hash.bcrypt(request.password)
    })
    db.commit()
    return 'ok'

def delete_user(db: Session, id: int):
    user= db.query(DbUser).filter(DbUser.id == id).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
            detail=f'User with {id} not found')
    db.delete(user)
    db.commit()
    return 'ok'