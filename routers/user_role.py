from routers.schemas import UserBase, UserRole, RoleBase, Role
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from database.database import get_db
from database import db_user_role
from typing import List
from auth.oauth2 import get_current_user

router = APIRouter(
    prefix='/user_role',
    tags=['user_role']
)

#create user role
@router.post('/', response_model=UserRole)
def create_role(request: UserRole, db: Session = Depends(get_db)):
    return db_user_role.create_user_role(db, request)

#Read all user role
@router.get('/', response_model=List[UserRole])
def get_all_roles(db: Session = Depends(get_db), current_user: UserBase = Depends(get_current_user)):
    return db_user_role.get_all_roles(db)
